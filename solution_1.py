def up():
    for i in range(5):
        print(" " * (5 - i - 1), end="")
        print("*" * (2 * i + 1))


def down():
    for i in range(4, -1, -1):
        print(" " * (5 - i - 1), end="")
        print("*" * (2 * i + 1))


up()
down()
