def up2():
    for i in range(5):
        print(" " * i, end="")
        print("\\", end="")
        print("o" * 2 * (4 - i), end="")
        print("/")


def down2():
    for i in range(5):
        print(" " * (4 - i), end="")
        print("/", end="")
        print("o" * 2 * i, end="")
        print("\\")


up2()
down2()
