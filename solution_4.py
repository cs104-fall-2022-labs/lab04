def is_prime_number(n):
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


print(is_prime_number(4))  # expect False
print(is_prime_number(7))  # expect True
